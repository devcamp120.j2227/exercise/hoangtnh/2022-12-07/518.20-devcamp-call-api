import { Component } from "react";

class FetchApi extends Component{
    //sử dụng async await
    fetchApi = async (url, body) => {
        const response = await fetch(url, body);

        const data = await response.json();
        return data;
    }


    getAllBtnHandler = () =>{
        console.log("Get All: ");

        //Call api get all sử dụng fetch
        // fetch("https://jsonplaceholder.typicode.com/posts")
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then((data)=>{
        //         console.log(data)
        //     })
        //     //Bắt lỗi
        //     .catch((error)=>{
        //         console.log(error.message)
        //     })

            this.fetchApi("https://jsonplaceholder.typicode.com/posts")
                .then((data)=>{
                    console.log(data)
                })
                .catch((error)=>{
                    console.log(error.message)
                })
    }

    getDetailBtnHandler = () =>{
        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1")
                .then((data)=>{
                    console.log(data)
                })
                .catch((error)=>{
                    console.log(error.message)
                })
    }

    createBtnHandler = () =>{
        const body =  {
            method:"POST",
            body: JSON.stringify({
                id: 1,
                title:"foo",
                body:"bar",
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },

        }
        this.fetchApi("https://jsonplaceholder.typicode.com/posts/", body)
                .then((data)=>{
                    console.log(data)
                })
                .catch((error)=>{
                    console.log(error.message)
                })
    }

    updateBtnHandler = () =>{
        const body = {
            method: 'PUT',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })

    }

    deleteBtnHandler =() => {
        const body = {
            method: 'DELETE'
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })

    }
    render(){
        return (
            <div className="row mt-5">
                <div className="col">
                    <button className="btn btn-info" onClick={this.getAllBtnHandler}>Get all</button>
                </div>
                <div className="col">
                    <button className="btn btn-info" onClick={this.getDetailBtnHandler}>Get detail</button>
                </div>
                <div className="col">
                    <button className="btn btn-info" onClick={this.createBtnHandler}>Create</button>
                </div>
                <div className="col">
                    <button className="btn btn-info" onClick={this.deleteBtnHandler}>Delete</button>
                </div>
                <div className="col">
                    <button className="btn btn-info" onClick={this.updateBtnHandler}>Update</button>
                </div>
            </div>
        )
    }
}
export default FetchApi;